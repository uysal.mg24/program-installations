## gitbook-plugin-page-footer-ex


##### Usage
Add it to your book.json:
```
{
    "plugins": [
        "page-footer-ex"
    ],
    "pluginsConfig": {
        "page-footer-ex": {
            "copyright": "By <em>author name</em>",
            "markdown": false,
            "update_label": "<i>updated</i>",
            "update_format": "YYYY-MM-DD HH:mm:ss"
        }
    }
}
```
or
```
{
    "plugins": [
        "page-footer-ex"
    ],
    "pluginsConfig": {
        "page-footer-ex": {
            "copyright": "[mrcode](https://github.com/zq99299)",
            "markdown": true,
            "update_label": "<i>updated</i>",
            "update_format": "YYYY-MM-DD HH:mm:ss"
        }
    }
}
```
##### Configuration Properties
```
{
      "copyright": {
        "type": "string",
        "default": "© All Rights Reserved",
        "title": "test",
        "description": "Copyright text"
      },
      "markdown": {
        "type": "boolean",
        "default": false,
        "title": "",
        "description": "Default false for plain text/HTML, true to parse copyright and update_label as Markdown"
      },
      "update_label": {
        "type": "string",
        "default": "updated",
        "title": "test",
        "description": "Date label"
      },
      "update_format": {
        "type": "string",
        "default": "YYYY-MM-DD HH:mm:ss",
        "title": "test",
        "description": "Moment.js date format"
      }
}
```
