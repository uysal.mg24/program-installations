## Book-Summary
Automatically creates a SUMMARY.md file for your book (mdbook/gitbook)


My initial intention is to get the chapters sorted without having to rename the chapter folders with a prefix number. The -s option takes the name of the chapters wich should come first. notes

#### Installation
OS X & Linux:
```
cargo install book-summary
```
```
git clone https://github.com/dvogt23/book-summary.git
cd book-summary
make install
```
#### Usage example
```
# create a SUMMARY.md file with custom sort in mdBook format
$ book-summary -n ./notes --sort tech personal
```

```
USAGE:
    book-summary [FLAGS] [OPTIONS]

FLAGS:
    -d, --debug        Activate debug mode
    -h, --help         Prints help information
    -m, --mdheader     Title from md file header?
    -V, --version      Prints version information
    -v, --verbose      Verbose mode (-v, -vv, -vvv)
    -y, --overwrite    Overwrite existing SUMMARY.md file

OPTIONS:
    -f, --format <format>            Format md/git book [default: md]
    -n, --notesdir <notesdir>        Notes dir where to parse all your notes from [default: ./]
    -o, --outputfile <outputfile>    Output file [default: SUMMARY.md]
    -s, --sort <sort>...             Start with following chapters
    -t, --title <title>              Title for summary [default: Summary]
```