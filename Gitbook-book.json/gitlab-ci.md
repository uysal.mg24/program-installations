gitlab runner add ssh-key remoteip.

master:
  script:
    - scp -r ./archive/ root@remoteip:/opt/gitbook/
  type: deploy
  only:
    - main
