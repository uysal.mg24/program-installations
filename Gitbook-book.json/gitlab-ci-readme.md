## GitBook

Opened for Gitbook Projects and automated sync tests.

##### Content Management

It shows how to synchronize and manage the files in the /opt folder on gitbook.medyatakip.com:4000.

`scp -r` is used as copy command on gitlab-ci.yml in this project.

The copy command works like this:
```
scp -r ./archive/ root@gitbook.medyatakip.com:/opt/gitbook/archive/"$CI_PROJECT_NAME".md
```

In this command, the first line after `scp -r` specifies the file in its own folder and if we want to get all the contents of this file, we just need to write the file name.

The second line, before the `@` specifies which user to connect to the remote linux. After the `@` specifies the path to the remote linux folder.

We set the name of the `readme.md` file created in the `"$CI_PROJECT_NAME".md` section as the project name and send it accordingly.

##### After copy operations and sync operations

Gitbook linux is connected and the commands are run in order by going under the `/opt/gitbook` folder where the files are created:
```
gitbook build
cp -R /opt/gitbook/SUMMARY.md /opt/gitbook/README.md

```

We ensure that the files transferred with the`gitbook build` process are synchronized on `summary.md` and adjusted automatically.

With the `cp -R` command, we ensure that the changes in the summary.md are synchronized into the introduction.

Thanks to the `gitbook serve` command, we run `gitbook` again.
