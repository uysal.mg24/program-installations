## plugin-github

Display a link to your GitHub repo in your gitbook.

##### Usage

Put this in your book.json:

```
{
    "plugins": [ "github" ],
    "pluginsConfig": {
        "github": {
            "url": "https://github.com/your/repo"
        }
    }
}
```
And you're done!