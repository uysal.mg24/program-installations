## gitbook-plugin-search-pro

You can search any characters(utf-8) and highlight it in your GitBook, not only english(exp:Chinese).

Note: Only gitbook >= 3.0.0 support

##### Usage
Before use this plugin, you should disable the default search plugin first, Here is a book.js configuration example:

```
{
    "plugins": [
      "-lunr", "-search", "search-pro"
    ]
}
```
Example
After installed gitbook.

```
    > git clone git@github.com:gitbook-plugins/gitbook-plugin-search-pro.git -b gh-pages
    > cd ./gitbook-plugin-search-pro
    > npm install
    > gitbook serve ./
```