##### gitbook-plugin-toggle-chapters

#### install it using.
To use it in your book, add this to book.json:
```
{
    "plugins": ["toggle-chapters"]
}
```

After adding, let's do the following.

```
$ npm install gitbook-plugin-toggle-chapters
```
or
```
gitbook build
gitbook serve
```




