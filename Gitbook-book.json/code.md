## Code plugin for GitBook
Code blocks are cool but can be cooler. This plugin adds lines numbers for multi-line blocks and a copy button to easily copy the content of your block.

#### How can I use this plugin?
You only have to edit your book.json and modify it adding something like this:
```
"plugins" : [ "code" ],
```
This will set up everything for you. If you want to get rid of the copy buttons use add this section too:
```
"pluginsConfig": {
  "code": {
    "copyButtons": false
  }
}
```
